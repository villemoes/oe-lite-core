FROM registry.gitlab.com/oe-lite/bakery
LABEL maintainer="Esben Haabendal <esben@haabendal.dk>"

ADD setup/debian_9 /tmp/oe-setup.sh
RUN sudo /bin/sh /tmp/oe-setup.sh \
 && sudo rm -rf /var/lib/apt/lists/*
