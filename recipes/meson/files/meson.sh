#!/bin/bash

# Do everything in a function to allow not clobbering an environment
# variable ($mesondir) that might have been set by the caller and
# expected to be passed through to meson. We could use
# sdllsasfng_sdfsf as a name instead, of course...
meson_wrapper() {
    local mesondir
    # We're in /usr/bin, meson is "installed" at
    # /usr/share/meson. Maybe without /usr, but that doesn't matter
    # for this logic.
    mesondir=$(realpath "${BASH_SOURCE[0]}") # ..../usr/bin/meson
    mesondir=$(dirname "${mesondir}")        # ..../usr/bin
    mesondir=$(dirname "${mesondir}")        # ..../usr
    mesondir="${mesondir}/share/meson"       # ..../usr/share/meson

    # Sanity check
    [ -d "${mesondir}" ] && \
    [ -d "${mesondir}/mesonbuild" ] && \
    [ -f "${mesondir}/meson.py" ] || \
	{ echo "expected meson installation in ${mesondir}" >&2 ; exit 1 ; }

    if [ -n "${PYTHONPATH}" ] ; then
	PYTHONPATH="${mesondir}:${PYTHONPATH}"
    else
	PYTHONPATH="${mesondir}"
    fi
    export PYTHONPATH
    exec python3 "${mesondir}/meson.py" "$@"
}
meson_wrapper "$@"
