DESCRIPTION = "Meson is an open source build system meant to be both extremely fast, and, even more importantly, as user friendly as possible."
HOMEPAGE = "https://mesonbuild.com/"
LICENSE = "Apache-2.0"

RECIPE_TYPES = "native"

# We don't have the infrastructure for installing native python
# modules into $D and then using them from the staging directory.
#
# Fortunately, meson supports being run directly from the source
# repo. So we just delete anything we certainly do not need at runtime
# (e.g. .git), install the rest at /usr/share/meson (which btw. is
# where most files end up in a Debian/Ubuntu "apt-get install meson"),
# and put a wrapper in /usr/bin/meson.

SRC_URI = "git://github.com/mesonbuild/meson.git;tag=${PV}"
SRC_URI += "file://meson.sh"
S = "${SRCDIR}/${PN}"

do_unpack_remove_cruft() {
    # Spaces in filenames, really? Why don't they throw in a couple of
    # UTF16-encoded emoji while at it.
    cd "${S}"
    rm -rf 'manual tests'
    rm -rf 'test cases'
    rm -rf .git .gitattributes .gitignore
    rm -rf .appveyor.yml .coveragerc .editorconfig .mailmap .travis.yml
    rm -rf ci ciimage docs
}
do_unpack[postfuncs] += "do_unpack_remove_cruft"

do_compile() {
    cd mesonbuild && python3 -m compileall .
}

do_install() {
    install -d "${D}${bindir}"
    install -d "${D}${datadir}/meson"
    install -m 0755 "${SRCDIR}/meson.sh" "${D}${bindir}/meson"
    cp -r . "${D}${datadir}/meson/"

    install -d "${D}${mandir}/man1"
    mv "${D}${datadir}/meson/man/"*.1 "${D}${mandir}/man1"
}
