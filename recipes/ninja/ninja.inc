SUMMARY = "Ninja is a small build system with a focus on speed."
HOMEPAGE = "https://ninja-build.org/"
LICENSE = "Apache-2.0"

RECIPE_TYPES = "native"

inherit c++

SRC_URI = "git://github.com/ninja-build/ninja.git;tag=v${PV}"
S = "${SRCDIR}/${PN}"

# The ./configure.py --bootstrap actually does all the work. I doubt
# anyone wants ninja on target, but one may need to be a little more
# clever to build ninja for an sdk.

do_configure() {
  :
}

do_compile() {
  ./configure.py --bootstrap
}

do_install() {
  install -d "${D}${bindir}"
  install -m 0755 ninja "${D}${bindir}"
}
