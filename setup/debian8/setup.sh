#!/bin/sh

set -e

echo "Installing required host packages"
apt-get update
apt-get install -y --no-install-recommends \
    $(: Archiving and compression) \
	bzip2 \
	cpio \
	gzip \
	lzma \
	lzop \
	tar \
	unzip \
	xz-utils \
	zip \
	zlib1g-dev \
    $(: Shell and core utilities) \
	bash \
	bash-completion \
	bc \
	coreutils \
	diffstat \
	diffutils \
	findutils \
	gawk \
	grep \
	less \
	sed \
    $(: Python) \
	python \
	python3 \
	python-dev \
	python-magic \
	python-pkg-resources \
	python-ply \
	python-pycurl \
	python-pysqlite2 \
	python-setuptools \
	python-svn \
    $(: Network) \
	curl \
	hostname \
	iproute2 \
	iputils-ping \
	wget \
	ssh \
    $(: Source management) \
	cvs \
	git-core \
	quilt \
	subversion \
    $(: Build infrastructure) \
	autoconf \
	automake \
	autopoint \
	make \
	pkg-config \
    $(: Compilers and friends) \
	binutils \
	bison \
	build-essential \
	cpp \
	chrpath \
	flex \
	g++ \
	gcc \
	gperf \
	libtool \
    $(: Doc tools) \
	asciidoc \
	docbook-utils \
	groff \
	gtk-doc-tools \
	help2man \
	libxml2-utils \
	openjade \
	texi2html \
	texinfo \
	xmlto \
    $(: System tools) \
	fakeroot \
	lsb-release \
	procps \
	sudo \
	util-linux \
    ;

echo "Installing OE-lite bakery"
git clone -b v4.2.0 https://gitlab.com/oe-lite/bakery.git /tmp/bakery
( cd /tmp/bakery && python setup.py install )
rm -rf /tmp/bakery
	
/bin/sh -c '[ -n "$BASH_VERSION" ]' || cat >&2 <<EOF
Warning: OE-lite expects /bin/sh to be bash, not dash. You can change this by running

  echo "dash dash/sh boolean false" | debconf-set-selections
  dpkg-reconfigure dash

as root.
EOF
